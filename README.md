# xfers_test

Coding skills assessment

## Evaluation Criteria

·Class and method names should clearly show their intent and responsibility
·Widgets should not know about business logic and non UI classes
·Your solution should easily accommodate possible future requirement changes
·Implement Unit Test cases. (For bonus points)
·Choose better architecture (BLoC recommended).
 
## Requirements:
1. Fetch a List of movies from this URL
http://api.themoviedb.org/3/search/movie?api_key=6753d9119b9627493ae129f3c3c99151&query=superman&page=1
 
2. Poster (size: w92, w185,w500)
http://image.tmdb.org/t/p/w92/2DtPSyODKWXluIRV7PVru0SSzja.jpg
 
3. Display result in ListView or GridView. The list would have the following rows 
Movie Image, Movie Name, Release date, Full description/Overview
 
4. Tapping on the cell should display the detail screen.
 
5. Pagination: when the user reaches the bottom of the list, 
it should load the second page if available.
