part of 'movies_cubit.dart';

@immutable
abstract class MoviesState {}

class MoviesInitial extends MoviesState {}
class MoviesLoaded extends MoviesState {
  final List<Movie> movies;

  MoviesLoaded(this.movies);
}

class MoviesLoading extends MoviesState {
  final List<Movie> loadedMovies;
  final bool isFirstFetch;

  MoviesLoading(this.loadedMovies, {this.isFirstFetch=false});
}