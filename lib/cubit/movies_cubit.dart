import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:xfers_test/data/models/movie.dart';
import 'package:xfers_test/data/repositories/movies_repository.dart';

part 'movies_state.dart';

class MoviesCubit extends Cubit<MoviesState> {
  MoviesCubit(this.repository) : super(MoviesInitial());

  int page = 1;
  final MoviesRepository repository;

  void loadPosts() {
    if (state is MoviesLoading) return;

    final currentState = state;

    var loadedMovies = <Movie>[];
    if (currentState is MoviesLoaded) {
      loadedMovies = currentState.movies;
    }

    emit(MoviesLoading(loadedMovies, isFirstFetch: page == 1));

    repository.fetchPosts(page).then((newMovies) {
      page++;

      final movies = (state as MoviesLoading).loadedMovies;
      movies.addAll(newMovies);

      emit(MoviesLoaded(movies));
    });
  }

}
