import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:xfers_test/cubit/movies_cubit.dart';
import 'package:xfers_test/data/models/movie.dart';
import 'package:xfers_test/presentation/movie_page.dart';

class HomePage extends StatelessWidget {
  final scrollController = ScrollController();

  void setupScrollController(context) {
    scrollController.addListener(() {
      if (scrollController.position.atEdge) {
        if (scrollController.position.pixels != 0) {
          BlocProvider.of<MoviesCubit>(context).loadPosts();
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    setupScrollController(context);
    BlocProvider.of<MoviesCubit>(context).loadPosts();

    return Scaffold(
      appBar: AppBar(
        title: Text("Movies for query 'superman'"),
      ),
      body: _movieList(),
    );
  }

  Widget _movieList() {
    return BlocBuilder<MoviesCubit, MoviesState>(builder: (context, state) {
      if (state is MoviesLoading && state.isFirstFetch) {
        return _loadingIndicator();
      }

      List<Movie> movies = [];
      bool isLoading = false;

      if (state is MoviesLoading) {
        movies = state.loadedMovies;
        isLoading = true;
      } else if (state is MoviesLoaded) {
        movies = state.movies;
      }

      return ListView.separated(
        controller: scrollController,
        itemBuilder: (context, index) {
          if (index < movies.length)
            return _movie(movies[index], context);
          else {
            Timer(Duration(milliseconds: 30), () {
              scrollController.jumpTo(scrollController.position.maxScrollExtent);
            });
            return _loadingIndicator();
          }
        },
        separatorBuilder: (context, index) {
          return Divider(
            color: Colors.grey[400],
          );
        },
        itemCount: movies.length + (isLoading ? 1 : 0),
      );
    });
  }

  Widget _loadingIndicator() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(child: CircularProgressIndicator()),
    );
  }

  Widget _movie(Movie movie, BuildContext context) {
    final image = movie.posterPathFor(92) == null
        ? SizedBox(
            width: 10,
            height: 10,
          )
        : Image.network(movie.posterPathFor(92));
    return ListTile(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => MoviePage(movie: movie)));
      },
      leading: image,
      title: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            '${movie.title}',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text(
                'Release date: ${movie.releaseDate}',
                style: TextStyle(fontStyle: FontStyle.normal),
              ),
              Text(
                '${movie.overview}',
                style: TextStyle(fontStyle: FontStyle.italic),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
