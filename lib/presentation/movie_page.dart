import 'package:flutter/material.dart';
import 'package:xfers_test/data/models/movie.dart';

class MoviePage extends StatelessWidget {
  Movie movie;

  MoviePage({Key key, @required this.movie}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width.toInt();
    final image = movie.posterPathFor(92) == null
        ? SizedBox(
            width: 10,
            height: 10,
          )
        : Image.network(movie.posterPathFor(width));
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        title: FittedBox(fit: BoxFit.fitWidth, child: Text(movie.title)),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              '${movie.title}',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text(
                  'Release date: ${movie.releaseDate}',
                  style: TextStyle(fontStyle: FontStyle.normal),
                ),
                Divider(),
                Stack(
                  children: [
                    image,
                    Text(
                      '${movie.overview}',
                      style: TextStyle(fontStyle: FontStyle.italic, color: Colors.black, backgroundColor: Colors.white),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
