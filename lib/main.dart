import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'cubit/movies_cubit.dart';
import 'data/repositories/movies_repository.dart';
import 'data/services/movies_service.dart';
import 'presentation/home_page.dart';

void main() {
  runApp(MyApp(
    repository: MoviesRepository(MoviesService()),
  ));
}

class MyApp extends StatelessWidget {
  final MoviesRepository repository;

  const MyApp({Key key, this.repository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BlocProvider(
        create: (context) => MoviesCubit(repository),
        child: HomePage(),
      ),
    );
  }
}
