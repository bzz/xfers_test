class Movie {
  final int id;
  final String title;
  final String releaseDate;
  final String overview;
  final String posterPath;

  Movie.fromJson(Map json)
      : id = json['id'] ?? "",
        title = json['title'] ?? "",
        releaseDate = json['release_date'] ?? "",
        overview = json['overview'] ?? "",
        posterPath = json['poster_path'] ?? "";

  String posterPathFor(int width) {
    if (width <= 92) {
      width = 92;
    } if (width <= 185) {
      width = 185;
    } else {
      width = 500;
    }
    if (posterPath == null || posterPath == "") return null;
    if (posterPath[0] != '/') return null;
    return 'http://image.tmdb.org/t/p/w$width' + posterPath;
  }
}
