import 'package:xfers_test/data/models/movie.dart';
import 'package:xfers_test/data/services/movies_service.dart';

class MoviesRepository {

  final MoviesService service;

  MoviesRepository(this.service);

  Future<List<Movie>> fetchPosts(int page) async {
    final posts = await service.fetchPosts(page);
    return posts.map((e) => Movie.fromJson(e)).toList();
  }
  
}