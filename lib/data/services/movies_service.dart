import 'dart:convert';
import 'package:http/http.dart';

class MoviesService {
  final url = "http://api.themoviedb.org/3/search/movie?api_key=6753d9119b9627493ae129f3c3c99151&query=superman";

  Future<List<dynamic>> fetchPosts(int page) async {
    try {
      final response = await get(Uri.parse(url + "&page=$page"));
      final Map<String, dynamic> allJson = jsonDecode(response.body);
      final List<dynamic> movieJson = allJson['results'];
      return movieJson;
    } catch (err) {
      return [];
    }
  }
}
