import 'package:flutter_test/flutter_test.dart';
import 'package:xfers_test/main.dart';

void main() {
  testWidgets('Visible text test', (WidgetTester tester) async {
    await tester.pumpWidget(MyApp());
    expect(find.text("Movies for query 'superman'"), findsOneWidget);
  });
}
